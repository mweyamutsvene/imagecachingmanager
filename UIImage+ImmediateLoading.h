//
//  UIImage+ImmediateLoading.h
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIImage (ImmediateLoading)

//Helper method for creating image
+ (UIImage*)imageImmediateLoadWithContentsOfFile:(NSString*)path;
- (UIImage*)initImmediateLoadWithContentsOfFile:(NSString*)path;

@end
