//
//  UIImage+Cached.m
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import "UIImageView+Cached.h"
#import <objc/runtime.h>

/*
 * Image cache interface
 */
@interface THImageCache : NSCache
- (UIImage *)cachedImageForRequest:(NSURLRequest *)request;
- (void)cacheImage:(UIImage *)image
        forRequest:(NSURLRequest *)request;
@end

@interface UIImageView (_Cached)

//Image cache
@property (readonly)THImageCache* th_sharedImageCache;
//Operation queue
@property (readonly)NSOperationQueue* th_sharedImageRequestOperationQueue;
@property (readwrite, nonatomic, strong, setter = th_setImageRequestOperation:) NSInvocationOperation *th_imageRequestOperation;


@end
static char kCTImageRequestOperationObjectKey;

@implementation UIImageView (Cached)
//Here we fetch an associated object
- (NSInvocationOperation *)th_imageRequestOperation {
    return (NSInvocationOperation *)objc_getAssociatedObject(self, &kCTImageRequestOperationObjectKey);
}

//Here we create a new property for UIImageView by adding an associated object
- (void)th_setImageRequestOperation:(NSURLSessionDataTask *)imageRequestOperation {
    objc_setAssociatedObject(self, &kCTImageRequestOperationObjectKey, imageRequestOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//Create a singleton for the inmemory store
+ (THImageCache *)th_sharedImageCache {
    static THImageCache *_th_imageCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _th_imageCache = [[THImageCache alloc] init];
    });
    
    return _th_imageCache;
}

//Create an operation queue for fetching cached images
+ (NSOperationQueue *)th_sharedImageRequestOperationQueue {
    static NSOperationQueue *_th_imageRequestOperationQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _th_imageRequestOperationQueue = [[NSOperationQueue alloc] init];
        [_th_imageRequestOperationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    });
    
    return _th_imageRequestOperationQueue;
}

#pragma mark - Private Methods
//Cancels and image chache operation
- (void)cancelCachedImageRequestOperation {
    [self cancelImageDownloadTask];

    [self.th_imageRequestOperation cancel];
    self.th_imageRequestOperation = nil;
}

//Loads an image from disk using immediate loading - if no image
//found, it will call the standard AFNetworking image loader
-(void)loadImageFromDisk:(NSURLRequest*)urlRequest
        placeholderImage:(UIImage *)placeholderImage
                 success:(THImageLoadCompletionBlock)success
                 failure:(THImageLoadFailureBlock)failure;
{
    
    //Load image from disk
    UIImage* image = [[THCacheManager sharedTHCacheManager] getCachedImageForURL:urlRequest.URL];
    
    if(image){
        //The image has been found and loaded -
        //Now cache it in memory
        [[[self class] th_sharedImageCache] cacheImage:image forRequest:urlRequest];
        dispatch_async(dispatch_get_main_queue(), ^{
            //Inform the men
            if(success){
                success(urlRequest,nil,image);
            }
            else{
                self.image = image;
            }
        });
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            __weak typeof(self) weakSelf = self;
            //Image has not been found, download it and cache it
            [self setImageWithURLRequest:urlRequest
                        placeholderImage:placeholderImage
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async([THCacheManager backgroundQueue], ^{
                                         if(request){
                                             //Cache image here in memory and to disk
                                             [[[weakSelf class] th_sharedImageCache] cacheImage:image forRequest:request];
                                             [[THCacheManager sharedTHCacheManager] cacheImage:image forURL:request.URL];
                                         }
                                     });
                                     if(success){
                                         success(request,response,image);

                                     }
                                     else{
                                         weakSelf.image = image;
                                     }

                                 }
                                 failure:failure];
        });
        
    }
    
}

#pragma mark - Public Methods
+(uint64_t)getFreeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

+(void)clearCachedImages;
{
    [[[self class] th_sharedImageCache] removeAllObjects];
}

+(UIImage*)cachedImageForURLRequest:(NSURLRequest*)urlRequest;
{
    return [[[self class] th_sharedImageCache] cachedImageForRequest:urlRequest];
}

+(void)cacheImage:(UIImage*)image toDiskWithURL:(NSURL*)url;
{
    
    if(!image || !url){
        return;
    }
        
    __strong UIImage* imageToCache = image.copy;
    dispatch_async([THCacheManager backgroundQueue], ^{
        [[THCacheManager sharedTHCacheManager] cacheImage:imageToCache forURL:url];
    });
    [[[self class] th_sharedImageCache] cacheImage:imageToCache forRequest:[NSURLRequest requestWithURL:url]];


}

- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                   fromCache:(BOOL)shouldCache
                       success:(THImageLoadCompletionBlock)success
                       failure:(THImageLoadFailureBlock)failure;

{
    self.image = nil;
    //Cancel any current operations on this imageview that haven't completed yet
    [self cancelCachedImageRequestOperation];
    
    //Cache when we need to
    if(shouldCache){
        //Check if this image has already been cached in memory
        UIImage *cachedImage = [[[self class] th_sharedImageCache] cachedImageForRequest:urlRequest];
        if(cachedImage ){
            [UIImageView cacheImage:cachedImage toDiskWithURL:urlRequest.URL];
            if(success){
                success(nil,nil,cachedImage);
            }
            else{
                self.image = cachedImage;
            }

            return;
        }
        
        if(![[THCacheManager sharedTHCacheManager] fileExistsForURL:urlRequest.URL]){
            __weak typeof(self) weakSelf = self;
            //Image has not been found, download it and cache it
            [self setImageWithURLRequest:urlRequest
                        placeholderImage:placeholderImage
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async([THCacheManager backgroundQueue], ^{
                                         if(request){
                                             //Cache image here in memory and to disk
                                             [[[weakSelf class] th_sharedImageCache] cacheImage:image forRequest:urlRequest];
                                             [[THCacheManager sharedTHCacheManager] cacheImage:image forURL:urlRequest.URL];
                                         }

                                     });

                                     if(success){
                                         success(request,response,image);
                                     }
                                     else{
                                         weakSelf.image = image;

                                     }
                                 }
                                 failure:failure];
            return;
        }
        //need to make a copy of the blocks before handing it over or they will become
        //nil before our eyes
        THImageLoadFailureBlock failureCopy = [failure copy];
        THImageLoadCompletionBlock successCopy = [success copy];
        
        //Create the Invocation operation and add it to the queue
        NSMethodSignature * mySignature = [[self class] instanceMethodSignatureForSelector:@selector(loadImageFromDisk:placeholderImage:success:failure:)];
        NSInvocation * myInvocation = [NSInvocation invocationWithMethodSignature:mySignature];
        [myInvocation setTarget:self];
        [myInvocation setSelector:@selector(loadImageFromDisk:placeholderImage:success:failure:)];
        [myInvocation setArgument:&urlRequest atIndex:2];
        [myInvocation setArgument:&placeholderImage atIndex:3];
        [myInvocation setArgument:&successCopy atIndex:4];
        [myInvocation setArgument:&failureCopy atIndex:5];
        
        self.th_imageRequestOperation = [[NSInvocationOperation alloc]
                                          initWithInvocation:myInvocation];
        [[self class].th_sharedImageRequestOperationQueue addOperation:self.th_imageRequestOperation];
        
    }
    else{
        //Fetch image without caching it
        [self setImageWithURLRequest:urlRequest
                    placeholderImage:placeholderImage
                             success:success
                             failure:failure];
    }
    
}

static inline NSString * ctImageCacheKeyFromURLRequest(NSURLRequest *request) {
    return [[request URL] absoluteString];
}
@end

@implementation THImageCache

- (UIImage *)cachedImageForRequest:(NSURLRequest *)request {
    switch ([request cachePolicy]) {
        case NSURLRequestReloadIgnoringCacheData:
        case NSURLRequestReloadIgnoringLocalAndRemoteCacheData:
            return nil;
        default:
            break;
    }
    
	return [self objectForKey:ctImageCacheKeyFromURLRequest(request)];
}

- (void)cacheImage:(UIImage *)image
        forRequest:(NSURLRequest *)request
{
    if (image && request) {
        [self setObject:image forKey:ctImageCacheKeyFromURLRequest(request)];
    }
}

@end

