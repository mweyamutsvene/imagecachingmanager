//
//  CTCacheManager.m
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import "THCacheManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "UIImage+ImmediateLoading.h"
#define CACHELIMIT 10
#define ONE_DAY 86400
#pragma mark - Private Interface
@interface THCacheManager ()
@end

@implementation THCacheManager
#pragma mark - Constructors
-(id)init{
    self = [super init];
    if(self == nil)
        return nil;
    
    //Clear all expired cache based on the |CACHELIMIT|
    self.daysToCache = CACHELIMIT;
    [self clearExpiredCache];
    
    return self;
}

-(void)dealloc{
}
#pragma mark - Accessors
SYNTHESIZE_SINGLETON_FOR_CLASS(THCacheManager);
static dispatch_queue_t __backgroundQueue;

+ (dispatch_queue_t)backgroundQueue {
    if (__backgroundQueue == nil)
        __backgroundQueue = dispatch_queue_create("com.myplate.images", NULL);
    return __backgroundQueue;
}

#pragma mark - Private Methods
- (NSString *)hashString:(NSString*)URLString {
    if(!URLString){
        return nil;
    }
    const char *cstr = URLString.UTF8String;
    unsigned char result[16];
    CC_MD5(cstr, (CC_LONG)strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
#pragma mark - Application's Cache directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

//Checks if a file at a given path is still valid
-(BOOL)isFileExpired:(NSString*)path;
{
    NSFileManager* manager = [NSFileManager defaultManager];
    NSDictionary* attributesOfFileAtPath = [manager attributesOfItemAtPath:path error:nil];
    NSDate* creationDate = [attributesOfFileAtPath objectForKey:NSFileCreationDate];
    NSDate* now = [NSDate date];
    NSDate* cacheLimit = [now dateByAddingTimeInterval:-ONE_DAY*self.daysToCache];
    if(creationDate.timeIntervalSince1970 < cacheLimit.timeIntervalSince1970){
        return true;
    }
    return false;
}

-(void)saveImage:(UIImage*)image toPath:(NSURL*)path;
{
    if(path == nil || image == nil){
        NSLog(@"Saving image - Path is nil");
        return;
    }

    //Write image to path, overwritting any existing image
    [UIImageJPEGRepresentation(image, 1.0) writeToURL:path atomically:true];
}

#pragma mark - Public Methods
-(BOOL)fileExistsForURL:(NSURL*)URL;
{
    NSString* md5Hash = [self hashString:URL.absoluteString];
    if (!md5Hash) {
        return NO;
    }
    NSURL* path = [[self applicationCacheDirectory] URLByAppendingPathComponent:md5Hash isDirectory:false];
    
    NSFileManager* manager = [NSFileManager defaultManager];
    return [manager fileExistsAtPath:path.path];
}

-(void)clearExpiredCache;
{
    NSFileManager* manager = [NSFileManager defaultManager];
    NSArray* contentsAtPath = [manager contentsOfDirectoryAtPath:[self applicationCacheDirectory].path error:nil];
    //Go through contents of a directory and delete all expired items
    for (NSString* path in contentsAtPath) {
        if([self isFileExpired:[[self applicationCacheDirectory].path stringByAppendingPathComponent:path]]){
            //Delete Cached Item
            NSError* error;
            [manager removeItemAtPath:[[self applicationCacheDirectory].path stringByAppendingPathComponent:path] error:&error];
        }
    }
}

-(UIImage*)getCachedImageForURL:(NSURL*)URL;
{
    NSString* md5Hash = [self hashString:URL.absoluteString];
    if (!md5Hash) {
        return nil;
    }
    NSURL* path = [[self applicationCacheDirectory] URLByAppendingPathComponent:md5Hash];
    
    //Check if file exists or is expired
    NSFileManager* manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path.path] && ![self isFileExpired:path.path]){
        //Update modified Attribute
        [manager setAttributes:@{NSFileModificationDate: [NSDate date]}
                  ofItemAtPath:path.path
                         error:nil];
        
        //File exists and is not expired
        UIImage* cachedImage = [UIImage imageImmediateLoadWithContentsOfFile:path.path];
        return cachedImage;
    }
    
    //clean up file
    [manager removeItemAtPath:path.path error:nil];
    return nil;
}

-(void)cacheImage:(UIImage*)image forURL:(NSURL*)URL;
{
    if(!URL){
        return;
    }
    NSString* md5Hash = [self hashString:URL.absoluteString];
    if (!md5Hash) {
        return;
    }
    NSURL* path = [[self applicationCacheDirectory] URLByAppendingPathComponent:md5Hash];
    
    [self saveImage:image toPath:path];
}

-(NSString*)getPathForURLString:(NSString*)URLString;
{
    NSString* md5Hash = [self hashString:URLString];
    if (!md5Hash) {
        return nil;
    }
    NSURL* path = [[self applicationCacheDirectory] URLByAppendingPathComponent:md5Hash];
    
    return path.relativePath;
    
    
}

-(void)deleteImageforURL:(NSURL*)URL;
{
    if(!URL){
        return;
    }
    NSString* md5Hash = [self hashString:URL.absoluteString];
    if (!md5Hash) {
        return;
    }
    NSURL* path = [[self applicationCacheDirectory] URLByAppendingPathComponent:md5Hash];
    NSFileManager* manager = [NSFileManager defaultManager];
    NSError* error;
    [manager removeItemAtURL:path error:&error];
    
    if(error){
        NSLog(@"error deleting:%@",error);

    }
    
    
}
@end
