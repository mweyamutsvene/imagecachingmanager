//
//  CTCacheManager.h
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SynthesizeSingleton.h"
@class THCacheManager;
@protocol THCacheManagerDelegate <NSObject>
@end

@interface THCacheManager : NSObject
SYNTHESIZE_SINGLETON_METHOD_FOR_CLASS(THCacheManager);
@property (assign) NSObject<THCacheManagerDelegate>* delegate;
@property (assign) long daysToCache;

+ (dispatch_queue_t)backgroundQueue;

//Clears all expired images from local cache
-(void)clearExpiredCache;

//Gets locally cached image for given URL
-(UIImage*)getCachedImageForURL:(NSURL*)URL;

//Caches an image based on the given URL
-(void)cacheImage:(UIImage*)image forURL:(NSURL*)URL;

//Deletes Image with given URL from Cache
-(void)deleteImageforURL:(NSURL*)URL;

//Gets the local storage path based on media URLString
-(NSString*)getPathForURLString:(NSString*)URLString;

//Checks if file exists
-(BOOL)fileExistsForURL:(NSURL*)URL;

@end
