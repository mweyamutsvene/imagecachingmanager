//
//  UIImage+Cached.h
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "THCacheManager.h"
/*
 This category allows fetching from disk first, and then if not found, we then fetch from the web using 
 AFNetworkings UIImageView category
 */
@interface UIImageView (Cached)
//Returns a cached image for a given URLRequest
+(UIImage*)cachedImageForURLRequest:(NSURLRequest*)urlRequest;

//Our completion Blocks
typedef void (^THImageLoadCompletionBlock)( NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image);
typedef void (^THImageLoadFailureBlock)( NSURLRequest *request, NSHTTPURLResponse *response, NSError *error);

//Sets the UIImageView's image with the option to cache it once fetched from the web
- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                   fromCache:(BOOL)fromCache
                       success:(THImageLoadCompletionBlock)success
                       failure:(THImageLoadFailureBlock)failure;



//If the file is already downloaded you can alternately use this method to cache the file
//to disk
+(void)cacheImage:(UIImage*)image toDiskWithURL:(NSURL*)url;
+(void)clearCachedImages;
+(void)cancelQueue;


@end
