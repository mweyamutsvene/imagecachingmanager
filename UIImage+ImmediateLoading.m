//
//  UIImage+ImmediateLoading.h
//
//  Created by Thomas Hanks on 5/28/13.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import "UIImage+ImmediateLoading.h"

@implementation UIImage (ImmediateLoading)

+ (UIImage*)imageImmediateLoadWithContentsOfFile:(NSString*)path {
  return [[UIImage alloc] initImmediateLoadWithContentsOfFile: path];
}

- (UIImage*) initImmediateLoadWithContentsOfFile:(NSString*)path {
  //Load image from path
  UIImage *image = [[UIImage alloc] initWithContentsOfFile:path];
  CGImageRef imageRef = [image CGImage];
  CGRect rect = CGRectMake(0.f, 0.f, CGImageGetWidth(imageRef), CGImageGetHeight(imageRef));
  
  //Creates a bitmap context for the image which allows lazy loading in the background- UIImage
  //Doesn't allow lazy loading in the background
   CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                     rect.size.width,
                                                     rect.size.height,
                                                     CGImageGetBitsPerComponent(imageRef),
                                                     CGImageGetBytesPerRow(imageRef),
                                                     CGImageGetColorSpace(imageRef),
                                                     kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little
                                                     );
  
  /*Dont mess with the kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little flags-
   They are needed to load the UIImage in the background*/
  
  //Draw the bitmap and create a new image from the bitmap
  CGContextDrawImage(bitmapContext, rect, imageRef);
  CGImageRef decompressedImageRef = CGBitmapContextCreateImage(bitmapContext);
  UIImage* decompressedImage = [[UIImage alloc] initWithCGImage: decompressedImageRef];
  
  //Clean up our mess
  CGImageRelease(decompressedImageRef);
  CGContextRelease(bitmapContext);
  
  return decompressedImage;
}

@end
